package com.ligz.dubbo.two.server.request;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * author:ligz
 */
@Data
@ToString
public class RecordPushRequest implements Serializable {
    //商品id
    private Integer itemId;
    //下单数量
    private Integer total;
    //客户的名称
    private String customerName;
}
