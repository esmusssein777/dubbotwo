package com.ligz.dubbo.two.server.data;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * author:ligz
 */
@Data
@ToString
public class DubboRecordResponse implements Serializable {
    private Integer code;

    private String msg;

    private Integer data;
}
