package com.ligz.dubbo.two.server.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ligz.dubbo.one.model.entity.OrderRecord;
import com.ligz.dubbo.two.server.data.DubboRecordResponse;
import com.ligz.dubbo.two.server.request.RecordPushRequest;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * author:ligz
 */
@Service
public class OrderRecordService {

    private static final Logger log = LoggerFactory.getLogger(OrderRecord.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired

    private HttpService httpService;

    private static final String url="http://127.0.0.1:9013/v1/record/push";//dubbo的rest接口

    private OkHttpClient httpClient=new OkHttpClient();

    /**
     * 处理controller层过来的用户下单数据
     * @param pushRequest
     * @throws Exception
     */
    public void pushOrder(RecordPushRequest pushRequest) throws Exception {
        try {
            //TODO:构造build
            Request.Builder builder = new Request.Builder().url(url)
                    .header("Content-type", "application/json");
            //TODO:构造请求头
            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),
                    objectMapper.writeValueAsString(pushRequest));
            //TODO:构造请求
            Request request = builder.post(requestBody).build();
            //TODO:发起请求
            Response response = httpClient.newCall(request).execute();
        }catch (Exception e) {
            throw e;
        }
    }

    /**
     * 处理controller层过来的用户下单数据-采用通用化http服务类实战
     * @param pushRequest
     * @throws Exception
     */
    public void pushOrderV2(RecordPushRequest pushRequest) throws Exception {
        try {
            Map<String, String> headerMap = new HashMap<>();
            headerMap.put("content-Type", "application/json");
            String res = httpService.post(url, headerMap, "application/json"
            , objectMapper.writeValueAsString(pushRequest));
            log.info("响应结果:{}", res);

            DubboRecordResponse dubboRecordResponse = objectMapper.readValue(res, DubboRecordResponse.class);
            log.info("得到的结果是:{}", dubboRecordResponse);
        }catch (Exception e) {
            throw e;
        }
    }
}
